<#
#Public Cheat Sheet
#
#>

#permanent AppX package removal
$killString = "*Office*"

#permanent AppX package removal
$packages = Get-AppxProvisionedPackage -online | where {$_.DisplayName -like $killString}
foreach ($package in $packages)
    {
    Remove-AppxProvisionedPackage -PackageName $package.PackageName -Online
    C:\Windows\System32\dism.exe /online /Remove-ProvisionedAppxPackage /PackageName:$($package.PackageName)
    }

$packages2 = Get-AppxPackage | where {$_.Name -like $killString}
foreach ($package2 in $packages2)
    {
    Remove-AppxPackage -Package $package2.PackageFullName
    Remove-AppxPackage -Package $package2.PackageFullName -AllUsers
    }